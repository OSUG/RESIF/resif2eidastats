# Gets 
# Needs environment variables :

# PGHOST: postgresql HOST
# PGDATABASE; postgresql database
# PGUSER: postgresql user
# PGPASSWORD: password for user
# EIDASTATS_DAYS: number of days to scan in the past
# EIDASTATS_TOKEN: token to use to send the payload
# EIDASTATS_URL: URL to send the payload to

# Which interval to take the data from : previous day
timeinterval="date_trunc('day', now() - interval '$EIDASTATS_DAYS day') AND date_trunc('day', now())" 
# List of days covered by the query
coverage=$(psql -qtA -c "select array_to_json(array_agg(distinct to_char(date, 'YYYY-mm-dd'))) from dataselectvol where date BETWEEN $timeinterval")
echo "Coverage computed: $coverage"
# Get all the statistics in JSON format
stats=$(
    psql -qtA -c "SELECT array_to_json(array_agg(row_to_json(t))) FROM (
           SELECT date_trunc('month', date) as month, network, station, location, channel, country,
                  sum(bytes) as bytes,
                  hll_add_agg(hll_hash_bigint(userid)) as clients,
                  count(distinct(requestid)) as nb_successful_requests,
                  count(distinct(requestid)) as nb_requests,
                  0 as nb_unsuccessful_requests
           FROM dataselectvol
           WHERE network is not NULL and date BETWEEN $timeinterval GROUP BY 1, 2, 3, 4, 5, 6)t;"
     )
unsuccessful_stats=$(
    psql -qtA -c "SELECT array_to_json(array_agg(row_to_json(t))) FROM (
           SELECT date_trunc('month', date) as month, '' as network, '' as station, '' as location, '' as channel, country,
                  sum(bytes) as bytes,
                  hll_add_agg(hll_hash_bigint(userid)) as clients,
                  0 as nb_successful_requests,
                  count(distinct(requestid)) as nb_requests,
                  count(distinct(requestid)) as nb_unsuccessful_requests
           FROM dataselectvol
           WHERE bytes = 0 and date BETWEEN $timeinterval GROUP BY 1, 2, 3, 4, 5, 6)t;"
                   )
version="0.5.3-resif"
#
echo $stats
# Encapsulate the statistics with metadata
# And send the result to the central webservice
echo "Sending statistics for successful requests to $EIDASTATS_URL"
echo '{"generated_at": "'$(date "+%Y-%m-%d %H:%M:%S")'", "version": "'$version'", "days_coverage": '$coverage', "aggregation_score": "0", "stats": '$stats'}' | curl --verbose --header "Authentication: Bearer ${EIDASTATS_TOKEN}"  --header "Content-Type: application/json" -d "@-"  $EIDASTATS_URL
echo '{"generated_at": "'$(date "+%Y-%m-%d %H:%M:%S")'", "version": "'$version'", "days_coverage": '$coverage', "aggregation_score": "0", "stats": '$unsuccessful_stats'}' | curl --verbose --header "Authentication: Bearer ${EIDASTATS_TOKEN}"  --header "Content-Type: application/json" -d "@-"  $EIDASTATS_URL
# echo "Done"
