from debian:10
RUN apt-get update && apt-get install -y curl postgresql-client
COPY src/resif2eidastats.sh /usr/local/bin
CMD ["/bin/bash", "/usr/local/bin/resif2eidastats.sh"]
